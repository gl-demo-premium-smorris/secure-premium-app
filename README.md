# Welcome to Sam's Premium demo project!

This is a static HTML/CSS site deployed to GitLab Pages site that is secured with Premium functionality.

Functionality includes:
- Script that parses through vulnerability report json and creates a merge request approval rule if a critical vulnerability is found
- Protected production environment
- Manual production deployment job
- Audit events
