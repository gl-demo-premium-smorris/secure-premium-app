import sys
import json
import gitlab

def load_report_details(json_filename):
    with open(json_filename) as json_file:
        report = json.load(json_file)
    return report    

def log_vulns(vulns):
    print("Vulnerabilities detected:")
    print(vulns)

def action_vulns(vulns, gl, project_id, commit_id):
    # Action each vulnerability
    for v in vulns:
        # Require approval if a critical vulnerability is found
        if v['severity'] == 'Critical':
            print("Critical vulnerability detected, approval required")
            CRITICAL_VULN_APPROVAL_RULE_NAME = "Critical vuln detected"
            
            # Get merge request object
            project = gl.projects.get(project_id)
            mr_id = project.commits.get(commit_id).merge_requests()[0].get('iid')
            mr = project.mergerequests.get(mr_id)

            # Check if rule already exists
            rule_exists = False
            for rule in mr.approval_rules.list():
                if getattr(rule, 'name') == CRITICAL_VULN_APPROVAL_RULE_NAME:
                    rule_exists = True

            # Create rule if it does not exist
            if not rule_exists:
                # Create an approval rule for the merge request
                mr.approval_rules.create({
                    "name": CRITICAL_VULN_APPROVAL_RULE_NAME,
                    "rule_type": "regular",
                    "approvals_required": 1,
                    "user_ids": [9142438]
                })

def determine_vulns(vulns, gl, project_id, commit_id):
    if not vulns:
        print("No vulnerabilities detected")
    else:
        log_vulns(vulns)
        action_vulns(vulns, gl, project_id, commit_id)

def authenticate_gitlab(project_accesss_token):
    # Use your project access token to authenticate for API calls
    # Ensure token has API access
    gl = gitlab.Gitlab('https://gitlab.com', private_token=project_accesss_token)
    print("Begin authenticating...")
    gl.auth()
    print("Authentication successful!")
    return gl

def main():
    # Get json filename
    json_filename = sys.argv[1]
    project_access_token = sys.argv[2]
    project_id = sys.argv[3]
    commit_id = sys.argv[4]
    
    # Generate report
    report = load_report_details(json_filename)
    vulns = report["vulnerabilities"]

    gl = authenticate_gitlab(project_access_token)
    determine_vulns(vulns, gl, project_id, commit_id)

main()